---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true

categories:
- Recipe
tags:
- Recipe
author: ''
prepTime: ''
cookTime: ''
ingredients: []
description: ''
resizeImages: false

---
